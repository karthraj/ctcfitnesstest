package Data;

/**
 * Created by supriya on 6/22/2016.
 */
public class Global {
    public static String protocol = "http";
    public static String server_ip = "52.27.216.21";
    //192.168.1.103 ,52.39.158.248
    public static String location = "ctc/fitness-test";
    public static String getTime_file = "getTime.php";
    public static String getEventDate_file = "getEventDate.php";
    public static String getData_file = "getData.php";
    public static String insertTime_file = "insertTime.php";
    public static String insertResults_file = "insertResults.php";
    public static String msg = "There is no data to sync";
    public static String success_msg = "Data synced successfully";
    public static String error_msg = "Unable to establish connection";
}
