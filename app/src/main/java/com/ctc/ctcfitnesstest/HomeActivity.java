package com.ctc.ctcfitnesstest;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;

import Data.Global;
import helper.DBController;
import helper.SyncActivity;

public class HomeActivity extends AppCompatActivity {

    DBController dbc = new DBController(this);
    private ProgressBar spinner;
    private ProgressDialog progress;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //dbc.deleteData();


        Button lst = (Button) findViewById(R.id.Button2);
        lst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, LogStartTimePage.class);
                startActivity(intent);
            }
        });

        Button let = (Button) findViewById(R.id.Button3);
        let.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, LogEndTimePage.class);
                startActivity(intent);
            }
        });

        Button sd = (Button) findViewById(R.id.Button4);
        final int[] sync_status = {0};
       spinner=(ProgressBar)findViewById(R.id.progressBar);
        spinner.getIndeterminateDrawable().setColorFilter(Color.parseColor("#d3d3d3"), android.graphics.PorterDuff.Mode.MULTIPLY);
        spinner.setVisibility(View.GONE);

        sd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog progress;
                spinner.setVisibility(View.VISIBLE);
                final String[] response = {""};

                if (android.os.Build.VERSION.SDK_INT > 7) {
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        SyncActivity sa = new SyncActivity();
                        if (!(dbc.hasData("emailId"))) {
                            response[0] = sa.getData(dbc,"",Global.getData_file);
                        }
                        else if (Integer.parseInt(sa.getEventDate(Global.getEventDate_file)) > dbc.getEventDate()) {
                            response[0] = sa.getData(dbc,"",Global.getData_file);
                        }
                        else if (sync_status[0] == 1) {
                            response[0] = Global.msg;
                        }
                        else {
                            JSONArray st_js = dbc.retrieveTime();
                            if(st_js!=null || st_js.length()>0) {
                                response[0] = sa.performPostCall(String.valueOf(st_js), Global.insertTime_file);
                            }
                            else{
                                response[0]=Global.msg;
                            }
                            if(response[0].contains("successfully")){
                               sync_status[0] =1;
                            }
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //progress.dismiss();
                                spinner.setVisibility(View.GONE);
                                Toast.makeText(HomeActivity.this,
                                        response[0], Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }).start();
            }
        });


        Button res = (Button) findViewById(R.id.Button5);
        res.setOnClickListener(new View.OnClickListener() {
            Intent intent = new Intent(HomeActivity.this, Results.class);
            @Override
            public void onClick(View view) {
                final ProgressDialog progress;
                spinner.setVisibility(View.VISIBLE);
                final String[] response = {""};

                if (android.os.Build.VERSION.SDK_INT > 9) {
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        SyncActivity sa = new SyncActivity();
                        if(!(dbc.hasData("result")))
                        {
                            if (dbc.hasData("startTime")) {
                                response[0] = sa.getData(dbc,"endTime",Global.getTime_file);
                            }
                            else{
                                response[0] = sa.getData(dbc,"startTime",Global.getTime_file);
                            }

                        }
                        startActivity(intent);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //progress.dismiss();
                                spinner.setVisibility(View.GONE);
                                Toast.makeText(HomeActivity.this,
                                        response[0], Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }).start();


            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }


}
