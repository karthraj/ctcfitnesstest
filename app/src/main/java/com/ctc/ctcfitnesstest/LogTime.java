package com.ctc.ctcfitnesstest;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by supriya on 3/6/2016.
 */
public class LogTime {

    public String getTime()
    {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(cal.getTime());
    }
}
