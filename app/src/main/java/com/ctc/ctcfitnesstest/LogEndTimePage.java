package com.ctc.ctcfitnesstest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;

import helper.DBController;
import helper.DynamicTable;

/**
 * Created by supriya on 3/6/2016.
 */
public class LogEndTimePage extends AppCompatActivity {

    DynamicTable dt = new DynamicTable();
    DBController dbc = new DBController(this);
    TableLayout table_layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int flag =1;
        String value = "log_end_time";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logendtime);
        //HorizontalScrollView sv = (HorizontalScrollView)findViewById(R.id.end__horizontal_scroller);
        table_layout = (TableLayout) findViewById(R.id.end_tableLayout);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        TextView name = (TextView)findViewById(R.id.end_name);
        TextView email = (TextView)findViewById(R.id.end_email);
        TextView time = (TextView)findViewById(R.id.end_time);

        name.setWidth((int)(width/2.4));
        email.setWidth((int) (width/2.4));
        time.setWidth((int)(width/2.4));
        ScrollView sv = (ScrollView) findViewById(R.id.end_scroller);
        dt.buildTable(table_layout, dbc, this,flag,value,sv,width);
    }

    }

