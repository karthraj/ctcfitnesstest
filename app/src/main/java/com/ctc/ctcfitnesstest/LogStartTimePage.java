package com.ctc.ctcfitnesstest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;

import helper.DBController;
import helper.DynamicTable;

/**
 * Created by supriya on 3/6/2016.
 */
public class LogStartTimePage extends AppCompatActivity {

    DynamicTable dt = new DynamicTable();
    DBController dbc = new DBController(this);

    TableLayout table_layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int flag =1;
        String value = "log_start_time";
       //dbc.deleteTable();

        //dbc.insertData("Zafron", "Hermoine@gmail.com");
        //dbc.insertData("ABC","Ron67894@gmail.com");
        //dbc.insertData("Ronaldo","Ronaldo67894@gmail.com");
        //dbc.insertData("freaky", "freaking_out1@gmail.com");

       /** dbc.setDate("Hermoine@gmail.com", 1462300200);
        dbc.setDate("Ron67894@gmail.com",1462300200);
        dbc.setDate("Ronaldo67894@gmail.com",1462300200);
        dbc.setDate("freaking_out1@gmail.com",1462041000); */

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logstarttime);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        TextView name = (TextView)findViewById(R.id.start_name);
        TextView email = (TextView)findViewById(R.id.start_email);
        TextView time = (TextView)findViewById(R.id.start_time);

        name.setWidth((int)(width/2.4));
        email.setWidth((int) (width/2.4));
        time.setWidth((int)(width/2.4));

        //HorizontalScrollView hsv = (HorizontalScrollView)findViewById(R.id.start__horizontal_scroller);
        ScrollView sv = (ScrollView)findViewById(R.id.start_scroller);
       table_layout = (TableLayout) findViewById(R.id.start_tableLayout);
        dt.buildTable(table_layout,dbc,this,flag,value,sv,width);

    }

}
