package helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ctc.ctcfitnesstest.LogTime;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by supriya on 3/6/2016.
 */
public class DBController extends SQLiteOpenHelper{

    LogTime lt = new LogTime();

    String event_date_query = "SELECT date FROM ft_result ORDER BY date DESC Limit 1";


    public DBController(Context context) {
        super(context, "ctc_fitnessTest.db", null, 1);
    }

    //creates table
    @Override
    public void onCreate(SQLiteDatabase db) {
        String query;
        query = "CREATE TABLE ft_result(userName TEXT, emailId BLOB, startTime BLOB, endTime BLOB, result BLOB, date INTEGER)";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query;
        query = "DROP TABLE IF EXISTS ft_result";
        db.execSQL(query);
        onCreate(db);
    }

    public void deleteTable()
    {
        SQLiteDatabase db= this.getWritableDatabase();
        String query;
        query = "DROP TABLE IF EXISTS ft_result";
        db.execSQL(query);
        onCreate(db);
    }

    //INSERT DATA
    public void insertData(String username, String mail,int date)
    {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("userName",username);
        values.put("emailId",mail);
        values.put("date",date);
        database.insert("ft_result", null, values);
        database.close();
    }

    //delete data from db
    public void deleteData()
    {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("delete from ft_result");
    }

    //update start time in db
    public void setStartTime(String emailId,String time){
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("UPDATE ft_result SET startTime=" + "'" + time + "'" + "where emailId=" + "'" +emailId+"'");
    }

    //update end time in db
    public void setEndTime(String emailId,String time){
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("UPDATE ft_result SET endTime=" + "'" + time + "'" + "where emailId=" + "'" +emailId+"'");

    }
    public void setResult(String emailId,String result){
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("UPDATE ft_result SET result=" + "'" + result + "'" + "where emailId=" + "'" + emailId + "'");
    }

    //get start Time from DB
    public String getStartTime(String emailId) {
        String start_time="";
        SQLiteDatabase database = this.getWritableDatabase();
        String selectQuery = "SELECT startTime FROM ft_result where emailId=" +"'"+ emailId +"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if(cursor.moveToFirst())
            start_time = cursor.getString(0);
        database.close();
        return start_time;
    }

    //get start Time from DB
    public String getEndTime(String emailId) {
        String end_time="";
        SQLiteDatabase database = this.getWritableDatabase();
        String selectQuery = "SELECT endTime FROM ft_result where emailId=" +"'"+ emailId +"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if(cursor.moveToFirst())
            end_time = cursor.getString(0);
        database.close();
        return end_time;
    }

    //get result from table
    public String getResult(String emailId) {
        String result="";
        SQLiteDatabase database = this.getWritableDatabase();
        String selectQuery = "SELECT result FROM ft_result where emailId=" +"'"+ emailId +"'";
        Cursor cursor = database.rawQuery(selectQuery, null);
        if(cursor.moveToFirst())
            result = cursor.getString(0);
        database.close();
        return result;
    }


    //calculate results
    public String calculateResult(String emailId)
    {
        String result = "";
        if(hasData("result",emailId))
        {
            result = getResult(emailId);
        }
        else {
            String start_time = getStartTime(emailId);
            String end_time = getEndTime(emailId);

            if (!(start_time == null) && !(end_time == null)) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd:mm:ss");
                try {
                    Date d1 = sdf.parse(start_time);
                    Date d2 = sdf.parse(end_time);
                    long r = d2.getTime() - d1.getTime();
                    long diffSec = r / 1000;
                    long min = diffSec / 60;
                    long sec = diffSec % 60;
                    result = String.valueOf(min) + ":" + String.valueOf(sec);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                result = "N/A";
            }
        }
        setResult(emailId,result);
        return result;
    }

    //Get user details from SQLite DB as Array List
    public ArrayList<String[]> getAllParticipant() {
        int date=0;
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<String[]> participantsList = new ArrayList<String[]>();
        String date_query = event_date_query;
        Cursor cursor1 = db.rawQuery(date_query, null);
        if(cursor1.moveToFirst()) {
            date = cursor1.getInt(0);
        }
        String selectQuery = "SELECT userName, emailId FROM ft_result where date="+date+
                " GROUP BY emailId ORDER BY userName ASC";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                String[] data = new String[5];
                data[0] = cursor.getString(0);
                data[1] = cursor.getString(1);
                participantsList.add(data);
            } while (cursor.moveToNext());
        }
        db.close();
        return participantsList;
    }

    public JSONArray retrieveTime() {
        JSONArray st_js = new JSONArray();
        String selectQuery = "SELECT emailId,startTime,endTime,date FROM ft_result WHERE date = ("+event_date_query+")";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        String time="";
        Boolean flag =true;
        int pos=0;
        if(hasData("startTime"))
        {
            time="startTime";
            pos=1;
        }
        else if(hasData("endTime")){
            time="endTime";
            pos=2;
        }
        else{
            flag=false;
        }
        if(flag) {
            try {
                if (cursor.moveToFirst()) {
                    do {
                        JSONObject jo = new JSONObject();
                        jo.put("emailId", cursor.getString(0));
                        jo.put(time, cursor.getString(pos));
                        jo.put("event_date",cursor.getString(3));
                        st_js.put(jo);
                    } while (cursor.moveToNext());
                }
            } catch (JSONException e) {
            }
        }
        database.close();
        return st_js;
    }

    public Boolean hasData(String value)
    {
        Boolean flag = false;
        String Query = "SELECT * FROM ft_result WHERE "+  value + " !=NULL OR " + value +" !=''" + "AND date = ("+event_date_query+")" ;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(Query, null);
        int count = cursor.getCount();
        if(count>0)
        {
            flag=true;
        }
        return flag;
    }

    public Boolean hasData(String value1,String value2)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Boolean flag = false;
        String Query = "SELECT " +value1+ " FROM ft_result WHERE emailId= '"+  value2 + "'" + "AND date = ("+event_date_query+")";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.moveToFirst())
        {
            if(cursor.getString(0)!=null) {
                String val = cursor.getString(0);
                System.out.print(val);
                flag=true;
            }

        }
        return flag;
    }


    public int getEventDate(){
        int event_date=0;
        String Query = event_date_query;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(Query, null);
        if(cursor.moveToFirst()) {
            event_date = cursor.getInt(0);
        }
        return event_date;
    }

    public void parseJsonAndInserData(String result) {
        try {
            JSONObject jObject = new JSONObject(result);
            JSONArray jArray = jObject.getJSONArray("result");
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jo = jArray.getJSONObject(i);
                // Pulling items from the array
                String name = jo.getString("name");
                String email_id = jo.getString("emailId");
                int date = jo.getInt("event_date");
                insertData(name, email_id, date);
            }
        } catch (JSONException e) {
            // Oops
        }
    }

    public void setTime(String result,String postDataParams) {
        try {
            JSONObject jObject = new JSONObject(result);
            JSONArray jArray = jObject.getJSONArray("result");
            for (int i = 0; i < jArray.length(); i++) {

                JSONObject jo = jArray.getJSONObject(i);
                // Pulling items from the array
                String email_id = jo.getString("emailId");
                String time = jo.getString(postDataParams);
                if (postDataParams == "startTime")
                    setStartTime(email_id, time);
                else
                    setEndTime(email_id, time);
            }
        }catch (JSONException e) {
            // Oops
        }
    }

    public JSONArray retrieveResult() {
        JSONArray st_js = new JSONArray();
        String selectQuery = "SELECT emailId,result,date FROM ft_result WHERE date = ("+event_date_query+")";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
            try {
                if (cursor.moveToFirst()) {
                    do {
                        JSONObject jo = new JSONObject();
                        jo.put("emailId", cursor.getString(0));
                        jo.put("result", cursor.getString(1));
                        jo.put("event_date",cursor.getString(2));
                        st_js.put(jo);
                    } while (cursor.moveToNext());
                }
            } catch (JSONException e) {
            }
        database.close();
        return st_js;
    }
}



