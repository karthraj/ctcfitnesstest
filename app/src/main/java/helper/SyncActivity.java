package helper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import Data.Global;

/**
 * Created by supriya on 5/29/2016.
 */
    public class SyncActivity {

    public HttpURLConnection setup(String method, String filename) {
        URL url;
        HttpURLConnection conn = null;
        String requestURL = Global.protocol + "://" + Global.server_ip + "/" + Global.location + "/" + filename;
        try {
            url = new URL(requestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod(method);
            conn.setDoInput(true);
            conn.setDoOutput(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

    public void writeData(HttpURLConnection conn, String params) {
        try {
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(params);
            writer.flush();
            writer.close();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getResponseCode(HttpURLConnection conn) {
        int code = 0;
        try {
            code = conn.getResponseCode();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return code;
    }

    public String[] readData(HttpURLConnection conn) {
        String line;
        String response_ar[] = new String[2];
        response_ar[0]="";
        try{
            if (getResponseCode(conn) == HttpsURLConnection.HTTP_OK) {
        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        while ((line = br.readLine()) != null) {
            response_ar[0] += line;
            response_ar[1] = Global.success_msg;
        }
    }else {
          response_ar[0] = "0";
        response_ar[1] = Global.error_msg;
    }
   }catch (Exception e) {
        e.printStackTrace();
     }
        return response_ar;
    }

    public String performPostCall(String postDataParams, String filename) {
        HttpURLConnection conn = setup("POST",filename);
        writeData(conn,postDataParams);
        String response[] = readData(conn);
        return response[1];
    }

    public String getEventDate(String filename) {
        HttpURLConnection conn = setup("POST",filename);
        String response[] = readData(conn);
        return response[0];
    }

    public String getData(DBController dbc,String postDataParams,String filename) {
        //HttpURLConnection conn = setup("POST", filename);
        String response[] = new String[2];
        if(filename.equals("getData.php")){
            HttpURLConnection conn = setup("GET", filename);
            response = readData(conn);
            dbc.parseJsonAndInserData(response[0]);
        }
        else if(filename.equals("getTime.php")) {
            HttpURLConnection conn = setup("POST", filename);
            writeData(conn, postDataParams);
            response = readData(conn);
            dbc.setTime(response[0],postDataParams);
        }

        return response[1];
    }
}
