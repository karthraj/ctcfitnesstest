package helper;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import Data.Global;

/**
 * Created by supriya on 3/11/2016.
 */
public class DynamicTable {

    public void buildTable(TableLayout table_layout,DBController dbc,Context c,int flag, final String val,ScrollView sv, int Width)
    {
        SyncActivity sa = new SyncActivity();
        final TableLayout.LayoutParams tableRowParams=
                new TableLayout.LayoutParams
                        (TableLayout.LayoutParams.FILL_PARENT,TableLayout.LayoutParams.FILL_PARENT);
        tableRowParams.setMargins(0, 12, 0,15);

        TableRow row = new TableRow(c);
        TextView header1 = new TextView(c);
        TextView header2 = new TextView(c);
        final int dWidth = (int)(Width/2.6);

        ArrayList<String[]> participantsList = dbc.getAllParticipant();
        int rows = participantsList.size();
        int cols = 4;

         // print username and email id
        for (int i = 0; i < rows; i++) {

            row = new TableRow(c);
            final int tv_count_ar[] = new int[rows];
            setTextView(row, c, participantsList.get(i)[0], "#000000", 0, (int) (dWidth));

            if(flag==1) {

                setTextView(row, c, participantsList.get(i)[1],"#000000",0,(int) (dWidth));
                row.setLayoutParams(tableRowParams);
                TextView name_field = (TextView) row.getVirtualChildAt(0);
                TextView email_field = (TextView) row.getChildAt(1);
                if (val.equalsIgnoreCase("log_start_time")) {
                    if (dbc.hasData("startTime", participantsList.get(i)[1])) {
                        setTextView(row, c, dbc.getStartTime(participantsList.get(i)[1]), "#FFFFFF", 0, (int) (dWidth));
                        setColor(row, tableRowParams, email_field, name_field);
                    }
                    else{
                        click(row, tv_count_ar[i], c, val, dWidth, email_field,name_field,tableRowParams);
                    }
                }
                else if (val.equalsIgnoreCase("log_end_time")) {
                    if (dbc.hasData("endTime", participantsList.get(i)[1])) {
                        setTextView(row, c, dbc.getEndTime(participantsList.get(i)[1]), "#FFFFFF", 0, (int) (dWidth));
                        setColor(row, tableRowParams,email_field,name_field);
                    }
                    else{
                        click(row,tv_count_ar[i],c,val,dWidth,email_field,name_field,tableRowParams);
                    }
                }

                tv_count_ar[i]=3;
            }
            else{
                setTextView(row, c, participantsList.get(i)[1],"#000000",10,(int) (dWidth));
                TextView email_field = (TextView) row.getChildAt(1);
                setTextView(row, c, dbc.calculateResult(email_field.getText().toString()), "#000000", 10, (int) (dWidth));
            }

            table_layout.addView(row);
        }
        JSONArray st_js = dbc.retrieveResult();
        sa.performPostCall(String.valueOf(st_js), Global.insertResults_file);
        dbc.close();

    }
 public void setTextView(TableRow row,Context c,String value, String color,int mar_bot,int dpWidth) {
     row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
             TableRow.LayoutParams.WRAP_CONTENT));
     TextView tv = new TextView(c);
     tv.setLayoutParams(new TableRow.LayoutParams(dpWidth,
             TableRow.LayoutParams.WRAP_CONTENT));
     LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)tv.getLayoutParams();
     params.setMargins(0, 0, 10, mar_bot);
     tv.setLayoutParams(params);
     tv.setTextSize(13);
     tv.setTextColor(Color.parseColor(color));
     tv.setText(value);
     row.addView(tv);
 }

    public void click(final TableRow x,  final int row_count,final Context y,final String val,final int dWidth,final TextView email_field,
                      final TextView name_field,final TableLayout.LayoutParams tableRowParams){
        x.setOnClickListener(new View.OnClickListener() {
                                 @Override
                                 public void onClick(View v) {
                                     if (row_count != 3) {

                                         Calendar cal = Calendar.getInstance();
                                         SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");

                                         String time = sdf.format(cal.getTime());

                                         DBController dbc = new DBController(y);


                                         if (val.equalsIgnoreCase("log_start_time")) {
                                             dbc.setStartTime(email_field.getText().toString(), time);
                                             setTextView(x, y, dbc.getStartTime(email_field.getText().toString()), "#FFFFFF", 0, (int) (dWidth));

                                         } else if (val.equalsIgnoreCase("log_end_time")) {
                                                 dbc.setEndTime(email_field.getText().toString(), time);
                                             setTextView(x, y, dbc.getEndTime(email_field.getText().toString()), "#FFFFFF", 0, (int) (dWidth));

                                         }

                                         setColor(x,tableRowParams,email_field,name_field);
                                         // x.setLayoutParams(tableRowParams);

                                         //row_count = 3;
                                     }
                                 }
                             }
        );
    }

    public void setColor(TableRow row,TableLayout.LayoutParams tableRowParams,TextView email_field, TextView name_field){
        row.setBackgroundColor(Color.parseColor("#009ca3"));
        email_field.setTextColor(Color.parseColor("#FFFFFF"));
        name_field.setTextColor(Color.parseColor("#FFFFFF"));
        row.setLayoutParams(tableRowParams);
    }


}
